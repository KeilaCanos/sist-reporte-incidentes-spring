package com.example.reporteincidentes.config;

import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig
{
    @Bean
    public GroupedOpenApi groupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("mesa-de-ayuda")
                .packagesToScan("com.example.reporteincidentes.controllers")
                .build();
    }

}
