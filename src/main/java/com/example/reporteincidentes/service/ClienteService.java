package com.example.reporteincidentes.service;

import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.ClienteDTO;
import com.RIcommons.entity.Cliente;
import com.example.reporteincidentes.repository.IClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService
{
    @Autowired
    private IClienteRepository clienteRepository;

    @Autowired
    private DTOsAssembler dtosAssembler;

    public List<ClienteDTO> obtenerClientes(){
        List<Cliente> clienteList = clienteRepository.findAll();
        List<ClienteDTO> clienteDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(clienteList)){
            for (Cliente c : clienteList){
                clienteDTOList.add(dtosAssembler.toClienteDTO(c));
            }
        }

        return clienteDTOList;
    }

    public ClienteDTO obtenerClientePorId(int id_cliente){
        ClienteDTO clienteDTO = dtosAssembler.toClienteDTO(clienteRepository.findById(id_cliente).orElseThrow());
        return clienteDTO;
    }

}
