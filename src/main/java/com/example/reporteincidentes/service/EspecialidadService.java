package com.example.reporteincidentes.service;

import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.EspecialidadDTO;
import com.RIcommons.entity.Especialidad;
import com.example.reporteincidentes.repository.IEspecialidadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspecialidadService
{
    @Autowired
    IEspecialidadRepository especialidadRepository;
    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<EspecialidadDTO> obtenerEspePorServicio(int id_servicio){
        List<Especialidad> especialidadList = especialidadRepository.findEspecialidadByIdServicio(id_servicio);
        List<EspecialidadDTO> especialidadDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(especialidadList)){
            for (Especialidad e : especialidadList){
                especialidadDTOList.add(dtOsAssembler.toEspecialidadDTO(e));
            }
        }
        return especialidadDTOList;
    }
}
