package com.example.reporteincidentes.service;


import com.RIcommons.assembler.EntityAssembler;
import com.RIcommons.dto.IncidenteDTO;
import com.RIcommons.entity.Incidente;
import com.example.reporteincidentes.repository.*;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDateTime;

@Service
@Transactional
public class IncidenteService
{
    @Autowired
    private IIncidenteRepository incidenteRepository;
    @Autowired
    private IServicioRepository servicioRepository;
    @Autowired
    private ITecnicoRepository tecnicoRepository;
    @Autowired
    private IEspecialidadRepository especialidadRepository;
    @Autowired
    private IClienteRepository clienteRepository;
    @Autowired
    private EntityAssembler entityAssembler;


    public void guardarIncidente(IncidenteDTO incidenteDTO){
        Incidente incidente = entityAssembler.toIncidente(incidenteDTO);

        incidente.setEstado("Ingresado");
        incidente.setDia_ingresado(LocalDateTime.now());
        incidente.setDia_finalizado(null);
        incidente.setTiempo(0);

        incidente.setServicio(servicioRepository.findById(incidenteDTO.getServicio()).orElseThrow());
        incidente.setTecnico(tecnicoRepository.findById(incidenteDTO.getTecnico()).orElseThrow());
        incidente.setEspecialidad(especialidadRepository.findById(incidenteDTO.getEspecialidad()).orElseThrow());
        incidente.setCliente(clienteRepository.findById(incidenteDTO.getCliente()).orElseThrow());

        incidenteRepository.save(incidente);
    }
}
