package com.example.reporteincidentes.service;

import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.TecnicoDTO;
import com.RIcommons.entity.Tecnico;
import com.example.reporteincidentes.repository.ITecnicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import java.util.ArrayList;
import java.util.List;


@Service
public class TecnicoService
{
    @Autowired
    ITecnicoRepository tecnicoRepository;
    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<TecnicoDTO> obtenerTecnicos(){
        List<Tecnico> tecnicoList = tecnicoRepository.findAll();
        List<TecnicoDTO> tecnicoDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tecnicoList)){
            for (Tecnico tec : tecnicoList){
                tecnicoDTOList.add(dtOsAssembler.toTecnicoDTO(tec));
            }
        }
        return tecnicoDTOList;
    }
}
