package com.example.reporteincidentes.service;

import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.TecnicoDTO;
import com.RIcommons.entity.Tecnico;
import com.example.reporteincidentes.repository.ITecnicoEspecialidadRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class TecnicoEspecialidadService
{
    @Autowired
    ITecnicoEspecialidadRespository tecnicoEspecialidadRespository;
    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<TecnicoDTO> obtenerTecnicosPorEspe(int id_especialidad){
        List<Tecnico> tecnicoList = tecnicoEspecialidadRespository.findTecnicoByIdEspecialidad(id_especialidad);
        List<TecnicoDTO> tecnicoDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(tecnicoList)){
            for (Tecnico t : tecnicoList){
                tecnicoDTOList.add(dtOsAssembler.toTecnicoDTO(t));
            }
        }
        return tecnicoDTOList;
    }
}
