package com.example.reporteincidentes.service;

import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.ServicioDTO;
import com.RIcommons.entity.Servicio;
import com.example.reporteincidentes.repository.IClienteServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteServicioService
{
    @Autowired
    IClienteServicioRepository clienteServicioRepository;

    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<ServicioDTO> obtenerServicioPorCliente(int id_cliente){
        List<ServicioDTO> servicioDTOList = new ArrayList<>();
        List<Servicio> servicioList = clienteServicioRepository.findServicioByIdCliente(id_cliente);
        if (!ObjectUtils.isEmpty(servicioList)){
            for (Servicio s : servicioList){
                servicioDTOList.add(dtOsAssembler.toServicioDTO(s));
            }
        }
        return servicioDTOList;
    }
}
