package com.example.reporteincidentes.controllers;

import com.RIcommons.dto.IncidenteDTO;
import com.example.reporteincidentes.service.IncidenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/incidentes")
public class IncidenteController
{
    @Autowired
    IncidenteService incidenteService;

    @PostMapping("/save")
    public ResponseEntity<String> saveIncidente(@RequestBody IncidenteDTO incidentedto)
    {
        incidenteService.guardarIncidente(incidentedto);
        return ResponseEntity.ok("El incidente fue creado con exito");
    }
}
