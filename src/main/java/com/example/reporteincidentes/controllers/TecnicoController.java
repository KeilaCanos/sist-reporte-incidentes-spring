package com.example.reporteincidentes.controllers;

import com.RIcommons.dto.TecnicoDTO;
import com.example.reporteincidentes.service.TecnicoEspecialidadService;
import com.example.reporteincidentes.service.TecnicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tecnicos")
public class TecnicoController
{
    @Autowired
    TecnicoService tecnicoService;
    @Autowired
    TecnicoEspecialidadService tecnicoEspecialidadService;

    @GetMapping("/all")
    public ResponseEntity<List<TecnicoDTO>> getTecnicos(){
        return new ResponseEntity<>(tecnicoService.obtenerTecnicos(), HttpStatus.OK);
    }

    @GetMapping("/especialidad/{id_especialidad}")
    public ResponseEntity<List<TecnicoDTO>> getTecnicosByEspecialidad(@PathVariable("id_especialidad") int id_especialidad){
       return new ResponseEntity<>(tecnicoEspecialidadService.obtenerTecnicosPorEspe(id_especialidad), HttpStatus.OK);
    }

}
