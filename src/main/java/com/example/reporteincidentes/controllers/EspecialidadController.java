package com.example.reporteincidentes.controllers;

import com.RIcommons.dto.EspecialidadDTO;
import com.example.reporteincidentes.service.EspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/especialidad")
public class EspecialidadController
{
    @Autowired
    EspecialidadService especialidadService;

    @GetMapping("/servicio/{id_servicio}")
    public ResponseEntity<List<EspecialidadDTO>> getEspePorServicio(@PathVariable("id_servicio") int id_servicio){
        return new ResponseEntity<>(especialidadService.obtenerEspePorServicio(id_servicio), HttpStatus.OK);
    }
}
