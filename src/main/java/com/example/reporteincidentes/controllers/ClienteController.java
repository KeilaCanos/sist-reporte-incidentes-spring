package com.example.reporteincidentes.controllers;

import com.RIcommons.dto.ClienteDTO;
import com.RIcommons.dto.ServicioDTO;
import com.example.reporteincidentes.service.ClienteService;
import com.example.reporteincidentes.service.ClienteServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/clientes")
public class ClienteController
{
    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteServicioService clienteServicioService;

    @GetMapping("/all")
    public ResponseEntity<List<ClienteDTO>> getClientes(){
        return new ResponseEntity<>(clienteService.obtenerClientes(), HttpStatus.OK);
    }

    @GetMapping("/{id_cliente}")
    public ResponseEntity<ClienteDTO> getCliente(@PathVariable("id_cliente") int id_cliente){
        return new ResponseEntity<>(clienteService.obtenerClientePorId(id_cliente), HttpStatus.OK);
    }

    @GetMapping("/servicios/{id_cliente}")
    public ResponseEntity<List<ServicioDTO>> getServicioPorCliente(@PathVariable("id_cliente") int id_cliente){
        return new ResponseEntity<>(clienteServicioService.obtenerServicioPorCliente(id_cliente), HttpStatus.OK);
    }


}
