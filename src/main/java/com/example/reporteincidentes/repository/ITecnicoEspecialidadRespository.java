package com.example.reporteincidentes.repository;


import com.RIcommons.entity.Tecnico;
import com.RIcommons.entity.TecnicoEspecialidad;
import com.RIcommons.entity.pk.TecnicoEspecialidadPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITecnicoEspecialidadRespository extends JpaRepository<TecnicoEspecialidad, TecnicoEspecialidadPK>
{

    @Query("SELECT t FROM TecnicoEspecialidad te JOIN te.tecnico t WHERE te.especialidad.id = :id_especialidad")
    List<Tecnico> findTecnicoByIdEspecialidad(@Param("id_especialidad") int id_especialidad);
}
