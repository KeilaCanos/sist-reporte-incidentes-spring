package com.example.reporteincidentes.repository;


import com.RIcommons.entity.ClienteServicio;
import com.RIcommons.entity.Servicio;
import com.RIcommons.entity.pk.ClienteServicioPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IClienteServicioRepository extends JpaRepository<ClienteServicio, ClienteServicioPK>
{
    @Query("SELECT s FROM ClienteServicio cs JOIN cs.servicio s WHERE cs.cliente.id = :id_cliente")
    List<Servicio> findServicioByIdCliente(@Param("id_cliente") int id_cliente);
}
