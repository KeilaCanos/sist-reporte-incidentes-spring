package com.example.reporteincidentes.repository;


import com.RIcommons.entity.Incidente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IIncidenteRepository extends JpaRepository<Incidente, Integer>
{
}
