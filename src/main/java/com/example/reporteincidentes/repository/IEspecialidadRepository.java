package com.example.reporteincidentes.repository;


import com.RIcommons.entity.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IEspecialidadRepository extends JpaRepository<Especialidad, Integer>
{
    @Query(value = "SELECT e.*" +
            " FROM ESPECIALIDAD e" +
            " WHERE e.id_servicio = :id_servicio", nativeQuery = true)
    List<Especialidad> findEspecialidadByIdServicio(@Param("id_servicio") int id_servicio);
}
